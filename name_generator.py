from data import adjectives, words, weapons
import random


class MarkovChainsGenerator:
    def __init__(self, word_list, tuple_size):
        self.start_list = []
        self.tuple_size = tuple_size
        self.word_list = word_list
        # Generate dictionary
        self.graph = self.generate_graph()

    def generate_word(self):
        # choose random start
        result = random.choice(self.start_list)
        # Choose next element based on start
        tuple = self.select_random_items(self.graph[result])
        while True:
            tuple = self.select_random_items(self.graph[tuple])
            if tuple == "":
                break
            last_character = tuple[-1]
            result += last_character
        return result

    def generate_graph(self):
        dic = {}
        for k in self.word_list:
            i = 0
            j = i + self.tuple_size
            self.start_list.append(k[i:j])
            if k[i:j] not in dic:
                dic[k[i:j]] = {}
            while j <= len(k):
                curr_tuple = k[i:j]
                next_tuple = k[i + 1:j + 1]
                # If not in dictionary we create entry
                if curr_tuple not in dic:
                    dic[curr_tuple] = {}
                # If transition not in dictionary we create and put weight 1
                if next_tuple not in dic[curr_tuple]:
                    dic[curr_tuple][next_tuple] = 1
                # If already created we add 1 to weight
                else:
                    dic[curr_tuple][next_tuple] += 1
                i += 1
                j += 1
            # If end tuple then create end character or increase
            if next_tuple not in dic:
                dic[next_tuple] = {}
            if "" not in dic[next_tuple]:
                dic[next_tuple][""] = 1
            else:
                dic[next_tuple][""] += 1
        return dic

    def select_random_items(self, items):
        rnd = random.random() * sum(items.values())
        for item in items:
            rnd -= items[item]
            if rnd < 0:
                return item

    def generate_weapon_name(self, weapon_type):
        markov_name = self.generate_word()
        # Retrieve possible weapon names
        weapon_names = weapons.get_weapon_type(weapon_type)
        dict_size = len(weapon_names['possible_names'])
        # Take random weapon name
        seed = random.randint(0, dict_size - 1)
        # Concatenate markov name + weapon name
        generated_name = "{} {}".format(
            markov_name, weapon_names['possible_names'][seed])
        # Read CSV adjectives files
        dict_size = len(adjectives.ADJECTIVES)
        seed = random.randint(0, dict_size - 1)
        # Add ajectives to final name
        generated_name += " " + adjectives.ADJECTIVES[seed]
        return generated_name


if __name__ == "__main__":
    # with open("names.txt", 'r') as f:
    #     liste = f.read().split(" ")
    # test = MarkovChainsGenerator(liste, 3)
    # print(type(liste))
    markov = MarkovChainsGenerator(words.WORDS, 3)
    print(markov.generate_weapon_name("heavy_sword"))

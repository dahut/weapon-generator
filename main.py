from PySide2 import QtWidgets, QtGui
from data import style, words, weapons

import name_generator
import random
import weapon_modifier


class WeaponGenerator(QtWidgets.QDialog):

    def __init__(self, parent=None):
        super().__init__()
        self.setFont("Arial")
        self.setStyleSheet(style.STYLE)

        # APPEL FONCTIONS
        weapon_categories = weapons.get_weapon_categories()
        weapon_types = weapons.get_weapon_types()

        # """ DEBUT WIDGETS"""
        # Définition de la fenètre
        self.setWindowTitle("Weapon Generator")
        self.setFixedSize(700, 400)

        # Création des bouttons "Generate name" & "Generate modifiers"
        button_name = QtWidgets.QPushButton("Generate name")
        button_modifiers = QtWidgets.QPushButton("Generate modifiers")

        button_name.clicked.connect(self.on_button_generate_name_clicked)
        button_modifiers.clicked.connect(self.on_button_generate_modifiers_clicked)

        button_font = QtGui.QFont("Times", 10, QtGui.QFont.Bold)
        button_name.setFont(button_font)
        button_modifiers.setFont(button_font)

        # Nom d'arme/ Catégorie d'arme/ Caractéristiques arme
        weapon_label_font = QtGui.QFont("Times", 14, italic=True)
        weapon_name_font = QtGui.QFont("Times", 16, QtGui.QFont.ExtraBold)
        weapon_modifiers_font = QtGui.QFont("Times", 8, italic=True)

        self.weapon_label = QtWidgets.QLabel()
        self.weapon_name = QtWidgets.QLabel()
        self.weapon_modifiers_bonuses = QtWidgets.QLabel()
        self.weapon_modifiers_maluses = QtWidgets.QLabel()

        self.weapon_modifiers_bonuses.setObjectName("weapon_modifiers_bonuses")
        self.weapon_modifiers_maluses.setObjectName("weapon_modifiers_maluses")

        self.weapon_label.setFont(weapon_label_font)
        self.weapon_name.setFont(weapon_name_font)
        self.weapon_modifiers_bonuses.setFont(weapon_modifiers_font)
        self.weapon_modifiers_maluses.setFont(weapon_modifiers_font)

        # Niveau de l'arme
        self.weapon_level_label = QtWidgets.QLabel()
        self.weapon_level = QtWidgets.QComboBox()
        self.weapon_level.addItems(["1", "2", "3", "4", "5"])
        self.weapon_level_label.setText("Niveau de l'arme")

        # Niveau du malus
        self.weapon_malus_label = QtWidgets.QLabel()
        self.weapon_malus = QtWidgets.QComboBox()
        self.weapon_malus.addItems(["0", "1", "2", "3", "4", "5"])
        self.weapon_malus_label.setText("Nombre de malus")
        # """ FIN WIDGETS"""

        # """ DEBUT LAYOUT """
        # Création des layouts
        outer_layout = QtWidgets.QVBoxLayout()  # Layout principal
        top_layout = QtWidgets.QHBoxLayout()  # Layout qui contient: checkboxes_layout; comboboxes_layout; weapon_info_layout
        checkboxes_layout = QtWidgets.QVBoxLayout()
        comboboxes_layout = QtWidgets.QVBoxLayout()
        weapon_info_layout = QtWidgets.QVBoxLayout()
        button_layout = QtWidgets.QHBoxLayout()  # Layout qui contient les bouttons de générations

        # Combinaison des layouts
        top_layout.addLayout(checkboxes_layout)
        top_layout.addLayout(comboboxes_layout)
        top_layout.addLayout(weapon_info_layout)
        outer_layout.addLayout(top_layout)
        outer_layout.addLayout(button_layout)

        # Remplissage des layouts
        self.weapon_types_checkboxes = {}
        for weapon_type, weapon_category in zip(
                weapon_types, weapon_categories):
            self.weapon_types_checkboxes[weapon_type] = QtWidgets.QCheckBox(weapon_category)
            checkboxes_layout.addWidget(self.weapon_types_checkboxes[weapon_type])

        button_layout.addStretch()
        button_layout.addWidget(button_name)
        button_layout.addWidget(button_modifiers)

        weapon_info_layout.addWidget(self.weapon_name)
        weapon_info_layout.addWidget(self.weapon_label)
        weapon_info_layout.addWidget(self.weapon_name)
        weapon_info_layout.addWidget(self.weapon_label)
        weapon_info_layout.addWidget(self.weapon_modifiers_bonuses)
        weapon_info_layout.addWidget(self.weapon_modifiers_maluses)
        weapon_info_layout.addStretch()

        comboboxes_layout.addWidget(self.weapon_level_label)
        comboboxes_layout.addWidget(self.weapon_level)
        comboboxes_layout.addWidget(self.weapon_malus_label)
        comboboxes_layout.addWidget(self.weapon_malus)
        comboboxes_layout.addStretch()

        # Caractéristiques des layout
        outer_layout.setContentsMargins(10, 30, 10, 10)
        weapon_info_layout.setContentsMargins(10, 0, 0, 0)
        weapon_info_layout.addStretch()
        top_layout.addStretch()

        self.setLayout(outer_layout)
        # """ FIN LAYOUT """

    def raise_error(self, text):
        parent = self
        title = "Error message"
        QtWidgets.QMessageBox.critical(parent, title, text)

    def on_button_generate_name_clicked(self):
        weapon_categories = weapons.get_weapon_categories()
        weapon_types = weapons.get_weapon_types()
        self.weapon_label.clear()
        self.weapon_name.clear()
        checked_weapons = {}
        for weapon_type, weapon_category in zip(
                weapon_types, weapon_categories):
            if self.weapon_types_checkboxes[weapon_type].isChecked():
                checked_weapons[weapon_type] = weapon_category
        if checked_weapons:
            selected_weapon = random.choice(list(checked_weapons.keys()))
            self.weapon_name.setText(markov.generate_weapon_name(selected_weapon))
            self.weapon_label.setText("Type d'arme: " + checked_weapons[selected_weapon])
        else:
            self.raise_error("Select at least one weapon")

    def on_button_generate_modifiers_clicked(self):
        levelselected = int(self.weapon_level.currentText())
        maluslevelselected = int(self.weapon_malus.currentText())
        raw_modifiers = weapon_modifier.generate_modifiers(
            levelselected, maluslevelselected)
        formated_modifiers_bonuses = weapon_modifier.format_modifiers(
            raw_modifiers["bonuses"])
        formated_modifiers_maluses = weapon_modifier.format_modifiers(
            raw_modifiers["maluses"])
        self.weapon_modifiers_bonuses.setText(formated_modifiers_bonuses)
        self.weapon_modifiers_maluses.setText(formated_modifiers_maluses)


if __name__ == "__main__":
    app = QtWidgets.QApplication()
    markov = name_generator.MarkovChainsGenerator(words.WORDS, 3)
    form = WeaponGenerator()
    form.exec_()

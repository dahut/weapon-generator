import random

from data import weapon_modifiers


def format_modifiers(modifiers):
    string = ""
    for k in modifiers.keys():
        string += "* {} {}\n".format(
            modifiers[k]["value"], modifiers[k]["text"])
    return string


def generate_modifiers(level, maluses=0):
    """Sortie: result["maluses"] et result["bonuses"]"""
    bonuses = level + maluses
    result = {}
    # Generate bonuses
    i = 0
    result["bonuses"] = {}
    while i < bonuses:
        random_key = random.choice(list(weapon_modifiers.WEAPON_MODIFIERS["bonuses"].keys()))
        random_value = weapon_modifiers.WEAPON_MODIFIERS["bonuses"][random_key]
        random_text = weapon_modifiers.WEAPON_MODIFIERS["modifiers_text"][random_key]
        if random_key in result:
            result["bonuses"][random_key]["value"] += random_value
        else:
            result["bonuses"][random_key] = {}
            result["bonuses"][random_key]["value"] = random_value
            result["bonuses"][random_key]["text"] = random_text
        i += 1
    # Generate maluses
    i = 0
    result["maluses"] = {}
    while i < maluses:
        random_key = random.choice(list(weapon_modifiers.WEAPON_MODIFIERS["maluses"].keys()))
        random_value = weapon_modifiers.WEAPON_MODIFIERS["maluses"][random_key]
        random_text = weapon_modifiers.WEAPON_MODIFIERS["modifiers_text"][random_key]
        # If rnadom_key already a bonus
        if random_key in result["bonuses"]:
            result["bonuses"][random_key]["value"] += random_value
            if result["bonuses"][random_key]["value"] == 0:
                result["bonuses"].pop(random_key)
        else:
            result["maluses"][random_key] = {}
            result["maluses"][random_key]["value"] = random_value
            result["maluses"][random_key]["text"] = random_text
        i += 1
    return result


if __name__ == "__main__":
    # print(format_modifiers(generate_modifiers(1, 3)))
    # print(json.dumps(generate_modifiers(4, 3), indent=2))
    var = generate_modifiers(1, 3)
    print(format_modifiers(var["maluses"]))

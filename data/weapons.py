WEAPONS = {
    "dagger": {
        "possible_names": [
            "La Dague",
            "le Poignard",
            "Le Couteau",
            "Le Kandjar",
            "Le Kriss",
            "Le Tanto"
        ],
        "special": "Ignore la Vigueur et triple les dégâts quand l'attaque est furtive",
        "category": "Dagues",
        "skill": "Armes légères"
    },
    "sword": {
        "possible_names": [
            "La Spatha",
            "L'Épée",
            "Le Sabre",
            "Le Glaive",
            "Le Khukuri",
            "La Colichemarde",
            "Le Cimeterre"
        ],
        "special": "+1 dégâts sur la Vigueur",
        "category": "Épées",
        "skill": "Armes légères"
    },
    "axe": {
        "possible_names": [
            "La Hache",
            "Le Francisque",
            "Le Tomahawk",
            "Le Labrys"
        ],
        "special": "+1 dégâts sur la Santé",
        "category": "Haches",
        "skill": "Armes légères"
    },
    "club": {
        "possible_names": [
            "Le Gourdin",
            "La Massue",
            "le Marteau",
            "La Matraque"
        ],
        "special": "20% de chances d'entraver quand dégâts sur Santé",
        "category": "Massues",
        "skill": "Armes légères"
    },
    "war_hammer": {
        "possible_names": [
            "le Nadziak",
            "Le Bec de corbin",
            "Le Marteau de guerre",
            "Le Maillet",
            "Le Totokia"
        ],
        "special": "40% de chances d'entraver quand dégâts sur Santé",
        "category": "Marteaux de guerre",
        "skill": "Armes lourdes"
    },
    "heavy_sword": {
        "possible_names": [
            "La Claymore",
            "Le Flamberge",
            "Le Dadao",
            "Le Nodachi",
            "L'Espadon"
        ],
        "special": "+2 dégâts sur Vigueur",
        "category": "Épées lourdes",
        "skill": "Armes lourdes"
    },
    "war_axe": {
        "possible_names": [
            "Hache de guerre"
        ],
        "special": "+2 dégâts sur Santé",
        "category": "Haches de guerre",
        "skill": "Armes lourdes"
    },
    "pike": {
        "possible_names": [
            "La Pique",
            "Le Guisarme",
            "La Pertuisane",
            "Le Yari",
            "La Lance"
        ],
        "special": "20% de chances d'ignorer la Vigueur",
        "category": "Piques",
        "skill": "Armes d'hast"
    },
    "halberd": {
        "possible_names": [
            "La Hallebarde",
            "L'Anicroche",
            "La Faux de guerre",
            "Le Bardiche",
            "Le Vouge"
        ],
        "special": "20% de chances de faire trébucher la cible quand dégâts sur Santé",
        "category": "Hallebardes",
        "skill": "Armes d'hast"
    },
    "bow": {
        "possible_names": [
            "L'Arc long",
            "L'Arc composite",
            "L'Arbalète"
        ],
        "special": "Ignore la Vigueur quand l'attaque est furtive.\nPeut attaquer à moyenne et grande distance",
        "category": "Arcs",
        "skill": "Armes de trait"
    },
    "propulsor": {
        "possible_names": [
            "Le Propulseur",
            "La Fronde",
            "La Sarbacane"
        ],
        "special": "+1 dégât sur la Vigueur.\nPeut attaquer à courte et moyenne distance",
        "category": "Propulseurs",
        "skill": "Armes de trait"
    }
}


def get_weapon_type(weapon_type):
    return WEAPONS[weapon_type]


def get_weapon_types():
    """Fonction qui renvoie une liste contenant tous les types d'armes:
    ['dagger', 'sword', 'axe'...]"""
    return list(WEAPONS.keys())


def get_weapon_categories():
    """Fonction qui renvoie une liste contenant tous les catégories d'armes:
    ['Dagues', 'Épées', 'Haches'...]"""
    return [val["category"] for val in WEAPONS.values()]

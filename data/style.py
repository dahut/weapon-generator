STYLE = """QWidget {
    color: #ffffff
}

QDialog {
    background-color:#333333
}

QComboBox {
    border-radius: 3px;
    padding: 1px 18px 1px 3px;
    min-width: 6em;
    color:#ffffff;
    background-color: #777777;
}

QComboBox QAbstractItemView {
    background-color: #777777;
    color: #ffffff;
    selection-background-color: #9e9e9e;
}

QPushButton{
    border-radius: 3px;
    padding: 5px 5px 5px 5px;
    background-color: #777777;
    color: #ffffff;
}

QPushButton:hover {
    background-color: #9e9e9e;
}

QCheckBox {
    color: #ffffff;

}

#weapon_modifiers_bonuses {
    color: #00ff00;
}
#weapon_modifiers_maluses {
    color: #ff0000;
}
"""
